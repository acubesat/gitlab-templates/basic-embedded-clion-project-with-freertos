/*******************************************************************************
  Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This file contains the "main" function for a project.

  Description:
    This file contains the "main" function for a project.  The
    "main" function calls the "SYS_Initialize" function to initialize the state
    machines of all modules in the system
 *******************************************************************************/

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "list.h"
#include "SEGGER_RTT.h"
#include "ATSAM_Logging.h"

// *****************************************************************************
// *****************************************************************************
// Section: Main Entry Point
// *****************************************************************************
// *****************************************************************************

volatile uint8_t pinval = 0;
volatile int xTask1 = 1;

void xTask1Code(void *pvParameters){

    for(;;){
        PIO_PinToggle(PIO_PIN_PA23);
//        //pinval = PIO_PinRead(PIO_PIN_PA23);
        vTaskDelay(pdMS_TO_TICKS(500));
    }

};

void xTask2Code(void *pvParameters){

    for(;;){
        //PIO_PinToggle(PIO_PIN_PA23);
        pinval = PIO_PinRead(PIO_PIN_PA23);

        PRINT_ATSAM("---Hello Summoners---", true); // print with new line at the end
        vTaskDelay(pdMS_TO_TICKS(500));

        PRINT_ATSAM("My credit card number is "); // print without new line
        uint32_t creditCardNumber = 98234823;
        PRINT_ATSAM(creditCardNumber, true);
        vTaskDelay(pdMS_TO_TICKS(500));

        PRINT_ATSAM("Penguins live in temperatures around "); PRINT_ATSAM(-59.7923823); PRINT_ATSAM(" degrees Celsius", true);
        vTaskDelay(pdMS_TO_TICKS(500));
    }

};


int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );

    xTaskCreate(xTask1Code, "Task1",1000, NULL, tskIDLE_PRIORITY + 1, NULL);
    xTaskCreate(xTask2Code, "Task2",1000, NULL, tskIDLE_PRIORITY + 1, NULL);

    vTaskStartScheduler();
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}






/*******************************************************************************
 End of File
*/
