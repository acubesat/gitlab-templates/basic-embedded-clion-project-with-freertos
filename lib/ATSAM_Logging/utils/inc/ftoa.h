#pragma once
#include <math.h>
#include <string.h>
#include "definitions.h"                // SYS function prototypes

size_t ftoa(float value, uint8_t decimals, char outputBuffer[]);
