#include "ftoa.h"

size_t ftoa(float value, uint8_t decimals, char outputBuffer[]) {
    int32_t intPart = (int32_t) value;
    float decimalPart = value - intPart;

    uint16_t power = 1;
    while(decimals--) {
        power *= 10;
    }

    uint32_t decimalToInt = fabsf(decimalPart * power);

    itoa(intPart, outputBuffer, 10);
    size_t index = strlen(outputBuffer);
    outputBuffer[index++] = '.';
    itoa(decimalToInt, outputBuffer+index, 10);

    return strlen(outputBuffer);
}
