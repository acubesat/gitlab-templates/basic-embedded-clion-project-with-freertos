#include "ATSAM_Logging.h"

void transmitUSART1(char* outputBuffer, size_t outputBufferLength) {
    USART1_Write(&outputBuffer[0], outputBufferLength);
    while(USART1_WriteIsBusy());
}

void printIntWithUSART1(int32_t numToPrint, bool printNewLine) {
    char outputBuffer[20] = "";
    itoa(numToPrint, outputBuffer, INT_TO_STRING_BASE);
    size_t outputBufferLength = strlen(outputBuffer);

    if(printNewLine) {
        outputBuffer[outputBufferLength++] = '\n';
    }

    transmitUSART1(&outputBuffer[0], outputBufferLength);
}

void printStringWithUSART1(char* message, bool printNewLine) {
    char outputBuffer[100] = "";
    size_t outputBufferLength = strlen(message);
    strncat(outputBuffer, message, outputBufferLength);

    if(printNewLine) {
        outputBuffer[outputBufferLength++] = '\n';
    }

    transmitUSART1(&outputBuffer[0], outputBufferLength);
}

void printFloatWithUSART1(float numToPrint, bool printNewLine) {
    char outputBuffer[20];
    ftoa(numToPrint, FLOAT_TO_STRING_DECIMALS_PRECISION, outputBuffer);
    size_t outputBufferLength = strlen(outputBuffer);

    if(printNewLine) {
        outputBuffer[outputBufferLength++] = '\n';
    }

    transmitUSART1(&outputBuffer[0], outputBufferLength);
}
