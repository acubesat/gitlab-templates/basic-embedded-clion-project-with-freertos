#pragma once

#include "definitions.h"                // SYS function prototypes
#include "ftoa.h"
#include <string.h>

void printIntWithUSART1(int32_t numToPrint, bool printNewLine);

void printStringWithUSART1(char *message, bool printNewLine);

void printFloatWithUSART1(float numToPrint, bool printNewLine);

#define ATSAM_PRINT1(message) _Generic(message, char*: printStringWithUSART1, float: printFloatWithUSART1, double: \
printFloatWithUSART1, default: printIntWithUSART1)(message, false) // yes that default: option can be dangerousr
#define ATSAM_PRINT2(message, printNewLine) _Generic(message, char*: printStringWithUSART1, float: printFloatWithUSART1, double: \
printFloatWithUSART1, default: printIntWithUSART1)(message, printNewLine)

#define GET_3RD_ARG(arg1, arg2, arg3, ...) arg3
#define ATSAM_PRINT_MACRO_CHOOSER(...) GET_3RD_ARG(__VA_ARGS__, ATSAM_PRINT2, ATSAM_PRINT1, )

#define PRINT_ATSAM(...) ATSAM_PRINT_MACRO_CHOOSER(__VA_ARGS__)(__VA_ARGS__)

#define INT_TO_STRING_BASE 10
#define FLOAT_TO_STRING_DECIMALS_PRECISION 3